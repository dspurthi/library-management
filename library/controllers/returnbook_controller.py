from library.services.book_service import book_service
from django.shortcuts import render
from rest_framework.parsers import JSONParser
from requests import request
from django.http import JsonResponse
from sqlalchemy import JSON
# from library.models.bookbank_model import Bookbank
# from library.models.issue_book_model import BorrowedBook
from library.models.returnbook_model import ReturnedBook
# from library.models.book_model import Book
from library.views.returnbook_view import ReturnedbooksSerializer
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
# from datetime import datetime
from library.services.returnbook_service import returnbook_service
from library.services.book_service import book_service

@csrf_exempt
def returnbook(request):
    if request.method =='GET':
        R_books = ReturnedBook.objects.all()
        serializer = ReturnedbooksSerializer(R_books,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        status = book_service.can_return_book(data)
        if status == False:
            return JsonResponse("You returned the book already !!!!!!",safe=False)
        returnbook_service.book_status_update(data)
        returnbook_service.total_books_borrowed_update(data)
        returnbook_service.calculate_fine(data)
        
        serializer =ReturnedbooksSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors,status=400)