from django.shortcuts import render
from pyrsistent import b
from rest_framework.parsers import JSONParser
from requests import request
from django.http import JsonResponse
from sqlalchemy import JSON
from library.models.issue_book_model import BorrowedBook
from library.models.bookbank_model import Bookbank
from library.views.issuebook_view import BorrowedbooksSerializer
from library.services.bookbank_service import bookbank_service
from library.services.book_service import book_service
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

@csrf_exempt
def issuebook(request):
    if request.method =='GET':
        b_books = BorrowedBook.objects.all()
        serializer = BorrowedbooksSerializer(b_books,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        book_status = book_service.can_issue_book(data)
        if book_status:
            bookbank_service.total_books_borrowed(data)
            bookbank_service.change_status_of_book(data)
            serializer =BorrowedbooksSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data,status=201)
            return JsonResponse(serializer.errors,status=400)
            
        else:
            return JsonResponse("Sorry book is not available to borrow at this moment !!!!",safe=False)