from django.shortcuts import render
from rest_framework.parsers import JSONParser
from requests import request
from django.http import JsonResponse
from sqlalchemy import JSON
from library.models.bookbank_model import Bookbank
from library.views.bookbank_view import BookbankSerializer
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime


@csrf_exempt
def create_bookbank(request):
    if request.method =='GET':
        book_bank = Bookbank.objects.all()
        serializer = BookbankSerializer(book_bank,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer =BookbankSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            # print(serializer.data)
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors,status=400)