from rest_framework.parsers import JSONParser
from requests import request
from django.http import JsonResponse
from sqlalchemy import JSON
from library.models.staff_model import Librarian
from library.views.staff_view import LibrarianSerializer
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def staff_controller(request):
    if request.method =='GET':
        staff = Librarian.objects.all()
        serializer = LibrarianSerializer(staff,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer =LibrarianSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors,status=400)