from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.views.generic import CreateView
from rest_framework.parsers import JSONParser
from requests import request
from django.http import JsonResponse
from sqlalchemy import JSON
from library.models.user_model import User
from library.views.user_view import UserSerializer
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from django.http import HttpResponse


@csrf_exempt
def user_controller(request):
    if request.method =='GET':
        users = User.objects.all()
        serializer = UserSerializer(users,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer =UserSerializer(data=data,many = True)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201,safe=False)
        return JsonResponse(serializer.errors,status=400)