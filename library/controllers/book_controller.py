from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from requests import request
from django.http import JsonResponse
from sqlalchemy import JSON
from library.models.book_model import Book
from library.views.book_view import BookSerializer
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def book_controller(request):
    if request.method =='GET':
        books = Book.objects.all()
        serializer = BookSerializer(books,many=True)
        return JsonResponse(serializer.data,safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer =BookSerializer(data=data,many = True)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201,safe=False)
        return JsonResponse(serializer.errors,status=400)