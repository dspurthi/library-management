from django.shortcuts import render
from html5lib import serializer
from rest_framework.parsers import JSONParser
from requests import request
from django.http import HttpResponse, JsonResponse
from library.models.issue_book_model import BorrowedBook
from library.views.issuebook_view import BorrowedbooksSerializer
from django.views.decorators.csrf import csrf_exempt

def user_books_register(request,pk):
    try:
        books = BorrowedBook.objects.all().filter(user_id=pk)
    except BorrowedBook.DoesNotExist:
        return HttpResponse("No Books issued to this user")
    if request.method == 'GET':
        books_list = []
        for book in books:
            serializer = BorrowedbooksSerializer(book)
            books_list.append(serializer.data)
        return JsonResponse(books_list,safe=False)