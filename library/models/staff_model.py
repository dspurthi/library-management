from django.db import models
import uuid

class Librarian(models.Model):
    id = models.UUIDField(primary_key=True,editable=False, unique=True,default=uuid.uuid4)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    e_mail = models.CharField(max_length=50,unique=True)
    contact = models.BigIntegerField()

    def __str__(self):
        return self.first_name