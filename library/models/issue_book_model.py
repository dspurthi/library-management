from random import choice
from click import option
from django.db import models
import uuid
import datetime
from library.models.user_model import User
from library.models.book_model import Book
from library.models.staff_model import Librarian

class BorrowedBook(models.Model):
    id = models.UUIDField(primary_key=True,editable=False,unique=True,default=uuid.uuid4)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    book = models.ForeignKey(Book,on_delete=models.CASCADE)
    issued_by = models.ForeignKey(Librarian,on_delete=models.CASCADE)
    issued_date = models.DateField(auto_now=True)
    opt =(('A','Active'),('C','Closed'))
    issue_status = models.CharField(max_length=1,default='A',choices=opt)

    def __str__(self):
        return str(self.id)