from django.db import models
import uuid
import datetime
from library.models.issue_book_model import BorrowedBook
from library.models.staff_model import Librarian

class ReturnedBook(models.Model):
    id = models.UUIDField(primary_key=True,editable=False,unique=True,default=uuid.uuid4)
    borrow_id = models.ForeignKey(BorrowedBook,on_delete=models.CASCADE)
    return_date = models.DateField(auto_now=True)
    accepted_by = models.ForeignKey(Librarian,on_delete=models.CASCADE)
    fine = models.BigIntegerField(default=0)
        
    def __str__(self):
        return str(self.id)