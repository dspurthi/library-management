from django.db import models
import uuid

class Book(models.Model):
    id = models.UUIDField(primary_key=True,editable=False,unique=True,default=uuid.uuid4)
    title = models.CharField(max_length=100)
    Author = models.CharField(max_length=100)
    Publisher = models.CharField(max_length=100)
    edition = models.CharField(max_length=100)
    pages = models.CharField(max_length=100)
    CHOICES =(
        ('A','Available'),
        ('NA','Not Available')
    )
    status = models.CharField(max_length=2, choices = CHOICES)
    available_departments = (
        ('CSE','Computer Science'),
        ('CIVIL','Civil Engineering'),
        ('EEE','Electrical & Electronics Engineering'),
        ('MECH','Mechanical Engineering'),
        ('ECE','Electronics and Communication Engineering'),
        ('IT','Information Technogoly'),
        ('PG','Post Graduate')
    )
    department = models.CharField(max_length=5,choices= available_departments)
    remarks = models.CharField(max_length=300)

    def __str__(self):
        return self.title