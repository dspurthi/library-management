from django.db import models
import uuid
import datetime
from library.models.user_model import User

class Bookbank(models.Model):
    id = models.UUIDField(primary_key=True,editable=False, unique=True,default=uuid.uuid4)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    CHOICES = (
        ('Staff', 'Faculty'),
        ('UG', 'Under Graduate Student'),
        ('PG', 'Post Graduate Student'),
    )
    user_type = models.CharField(max_length=300, choices = CHOICES)
    joining_date = models.DateField(auto_now=True)
    due_amount = models.BigIntegerField(default=0)
    total_books_borrowed = models.BigIntegerField(default=0)
    # total_books_returned = models.BigIntegerField(default=0)

    def __str__(self):
        return str(self.id)