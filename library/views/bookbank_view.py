from rest_framework import serializers
from library.models.bookbank_model import Bookbank

class BookbankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bookbank
        fields = '__all__'