from rest_framework import serializers
from library.models.returnbook_model import ReturnedBook

class ReturnedbooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReturnedBook
        fields = ('id','borrow_id','return_date','accepted_by','fine')