from rest_framework import serializers
from library.models.issue_book_model import BorrowedBook


class BorrowedbooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = BorrowedBook
        fields = '__all__'