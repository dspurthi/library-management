from rest_framework import serializers
from library.models.staff_model import Librarian

class LibrarianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Librarian
        fields = '__all__'