from django.contrib import admin

# Register your models here.
from library.models.user_model import User
from library.models.book_model import Book
from library.models.staff_model import Librarian
from library.models.bookbank_model import Bookbank
from library.models.issue_book_model import BorrowedBook
from library.models.returnbook_model import ReturnedBook

admin.site.register(User)
admin.site.register(Book)
admin.site.register(Bookbank)
admin.site.register(BorrowedBook)
admin.site.register(ReturnedBook)
admin.site.register(Librarian)