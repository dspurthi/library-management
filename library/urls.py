from unicodedata import name
from django.urls import path
from library.controllers.user_controller import user_controller
from library.controllers.book_controller import book_controller
from library.controllers.staff_controller import staff_controller
from library.controllers.bookbank_controller import create_bookbank
from library.controllers.issuebook_controller import issuebook
from library.controllers.returnbook_controller import returnbook
from library.controllers.books_user_controller import user_books_register

from . import views

urlpatterns = [
    # path('', views.index, name='index'),
    path('users/', user_controller, name='user'),
    path('books/', book_controller, name='books'),
    path('staff/', staff_controller, name='librarian'),
    path('issue_book/', issuebook, name='borrowedbooks'),
    path('return_book/', returnbook, name='returnedbooks'),
    path('bookbank/', create_bookbank, name='bookbank'),
    path('userbooks_register/<uuid:pk>/', user_books_register, name='user_books_register'),
    # path('register/success/', views.success, name='success'),
    # path('register/user', views.UserCreateView.as_view(model=User,
    #      success_url="success/"), name='UserCreateView'),
    # path('register/book', views.BookCreateView.as_view(model=Book,
    #      success_url="success/"), name='BookCreateView'),
    # path('register/librarian', views.LibrarianCreateView.as_view(model=Librarian,
    #      success_url="success/"), name='LibrarianCreateView'),

]
