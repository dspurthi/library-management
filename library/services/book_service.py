from ast import Pass

from django.http import JsonResponse
from library.models.book_model import Book
from library.models.issue_book_model import BorrowedBook

class book_service:
    def can_issue_book(data):
        book = Book.objects.get(id=data.get('book'))
        if book.status == 'A':
            return True
        else:
            return False


    def can_return_book(data):
        book = BorrowedBook.objects.get(id=data.get('borrow_id'))
        if book.issue_status == 'C':
            return False
        else:
            return True