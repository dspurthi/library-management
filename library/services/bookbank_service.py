from library.models.bookbank_model import Bookbank
from library.models.book_model import Book

class bookbank_service:
    def total_books_borrowed(data):
        print(data)
        user = data.get('user')
        b_bookbank = Bookbank.objects.get(user=data.get('user'))
        b_bookbank.total_books_borrowed += 1
        b_bookbank.save()
        data['total_books_borrowed'] = b_bookbank.total_books_borrowed

    def change_status_of_book(data):
        # Book.objects.filter(id = self.book.id).update(status ='NA')
        req_book = Book.objects.get(id=data.get('book'))
        req_book.status="NA"
        req_book.save()
        # return (req_book.status)

# update_bookbank.total_books_borrowed = staticmethod(update_bookbank.total_books_borrowed)