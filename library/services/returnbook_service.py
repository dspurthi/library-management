from library.models.bookbank_model import Bookbank
from library.models.book_model import Book
from library.models.issue_book_model import BorrowedBook
from datetime import datetime


class returnbook_service:
    def get_issuedbook_details(data):
        issue_id = data.get('borrow_id')
        borrowed_book = BorrowedBook.objects.filter(id = issue_id)
        return borrowed_book

    def book_status_update(data):
        borrowed_book = returnbook_service.get_issuedbook_details(data)
        book = [book.book_id for book in borrowed_book]
        update_book = Book.objects.get(id = book[0])
        update_book.status= 'A'
        update_book.save()
        data['status'] = update_book.status

    def total_books_borrowed_update(data):
        borrowed_book = returnbook_service.get_issuedbook_details(data)
        user = [user.user_id for user in borrowed_book]
        bookbank_user = Bookbank.objects.get(user = user[0])
        bookbank_user.total_books_borrowed -= 1
        # bookbank_user.total_books_returned += 1
        bookbank_user.save()
        data['total_books_borrowed'] = bookbank_user.total_books_borrowed

    def issue_book_status_update(data):
        borrowed_book = returnbook_service.get_issuedbook_details(data)
        borrowed_book.issue_book_status = 'C'
        borrowed_book.save()
        
        
    
    def calculate_fine(data):
        borrowed_book = returnbook_service.get_issuedbook_details(data)
        r_date = datetime.today().strftime('%Y-%m-%d')
        print(r_date)
        i_date = [book.issued_date for book in borrowed_book]
        i_date = str(i_date[0])
        print(i_date)
        r_date = datetime.strptime(r_date, "%Y-%m-%d")
        i_date = datetime.strptime(i_date, "%Y-%m-%d")
        fine_amount = 0
        
        gap = abs((r_date - i_date).days)
        if gap > 14:
            penalty = (gap -14) * 5
            if penalty < 250:
                data['fine'] = penalty
                fine_amount = penalty
                # return penalty

            else:
                data['fine'] = 250 
                fine_amount = 250
                # penalty = 250
                # return penalty
        user = [user.user_id for user in borrowed_book]
        bookbank_user = Bookbank.objects.get(user = user[0])
        bookbank_user.due_amount += fine_amount
        bookbank_user.save()